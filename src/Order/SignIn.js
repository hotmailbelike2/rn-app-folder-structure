// @flow
import React, { Component } from 'react';
import { AppRegistry, StyleSheet,  View, Alert } from 'react-native';
import {
  GoogleSignin,
  GoogleSigninButton,
  statusCodes,
} from '@react-native-community/google-signin';
import type { User } from '@react-native-community/google-signin';
import {
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Form,
  List,
  Left,
  Right,
  Icon,
  Title,
  Button,
  Text,
  Input,
  Item,
  ListItem,
  Accordion,
  Toast,
  Root
} from 'native-base'
import config from './config'; // see docs/CONTRIBUTING.md for details
// import { TokenClearingView } from './TokenClearingView';
import { withNavigationFocus } from 'react-navigation'

type ErrorWithCode = Error & { code?: string };

type State = {
  error: ?ErrorWithCode,
  userInfo: ?User,
};

class GoogleSigninSampleApp extends Component<{}, State> {
  state = {
    userInfo: null,
    error: null,
    accessToken: 'hmm'
  };

  async componentDidMount() {
    this._configureGoogleSignIn();
    await this._getCurrentUser();
 
  }

  _configureGoogleSignIn() {
    GoogleSignin.configure({
      scopes:['https://www.googleapis.com/auth/drive','https://www.googleapis.com/auth/drive.appdata','https://www.googleapis.com/auth/drive.file'],
       webClientId: "639411709075-kcihmitrli8thc9ash8js2pshs7ku8md.apps.googleusercontent.com",//'639411709075-kcihmitrli8thc9ash8js2pshs7ku8md.apps.googleusercontent.com',//config.webClientId, //"639411709075-kcihmitrli8thc9ash8js2pshs7ku8md.apps.googleusercontent.com",//config.webClientId,
      offlineAccess: false,
    });
  }

  async _getCurrentUser() {
    try {
      const userInfo = await GoogleSignin.signInSilently();
  //    alert(JSON.stringify(userInfo))
      console.log("fdsfds",userInfo)

      this.setState({ userInfo, error: null },async () =>{
        const isSignedIn = await GoogleSignin.getTokens();
        this.setState({accessToken: isSignedIn.accessToken},()=>{alert("LOL")})
      });
    } catch (error) {
      const errorMessage =
        error.code === statusCodes.SIGN_IN_REQUIRED ? 'Please sign in :)' : error.message;
      this.setState({
        error: new Error(errorMessage),
      });
    }
  }

  render() {
    const { userInfo } = this.state;

    const body = userInfo ? this.renderUserInfo(userInfo) : this.renderSignInButton();
    return (
      <View style={[styles.container, styles.pageContainer]}>
        {this.renderIsSignedIn()}
        {this.renderGetCurrentUser()}
        {this.renderGetTokens()}
        <Button block iconRight success  onPress={() => { this.props.navigation.navigate('GoogleDrive')}}>

     <Text>Go To Drive</Text></Button>
     <Button block iconRight success  onPress={() => { console.log("access token: ", this.state.accessToken ); this.props.navigation.navigate('DriveExample',{accessToken: this.state.accessToken})}}>

<Text>Example </Text></Button>
        {body}
      </View>
    );
  }

  renderIsSignedIn() {
    return (
      <Button
        onPress={async () => {
          const isSignedIn = await GoogleSignin.isSignedIn();
          Alert.alert(String(isSignedIn));
        }}
        title="is user signed in?"
      />
    );
  }

  renderGetCurrentUser() {
    return (
      <Button
        onPress={async () => {
          const userInfo = await GoogleSignin.getCurrentUser();
          Alert.alert('current user', userInfo ? JSON.stringify(userInfo.user) : 'null');
        }}
        title="get current user"
      />
    );
  }

  renderGetTokens() {
    return (
      <Button
        onPress={async () => {
          const isSignedIn = await GoogleSignin.getTokens();
          this.setState({accessToken: isSignedIn.accessToken},()=>{alert("LOL")})
      //    Alert.alert('tokens', JSON.stringify(isSignedIn));
        }}
        title="get tokens"
      />
    );
  }

  renderUserInfo(userInfo) {
    return (
      <View style={styles.container}>
        <Text style={styles.userInfo}>Welcome {userInfo.user.name}</Text>
        <Text>Your user info: {JSON.stringify(userInfo.user)}</Text>
        {/* <TokenClearingView userInfo={userInfo} /> */}

        <Button onPress={this._signOut} title="Log out" />
        {this.renderError()}
      </View>
    );
  }

  renderSignInButton() {
    return (
      <View style={styles.container}>
        <GoogleSigninButton
          size={GoogleSigninButton.Size.Standard}
          color={GoogleSigninButton.Color.Auto}
          onPress={this._signIn}
        />
        {this.renderError()}
      </View>
    );
  }

  renderError() {
    const { error } = this.state;
    if (!error) {
      return null;
    }
    const text = `${error.toString()} ${error.code ? error.code : ''}`;
    return <Text>{text}</Text>;
  }

  _signIn = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      this.setState({ userInfo, error: null });
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // sign in was cancelled
        Alert.alert('cancelled');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation in progress already
        Alert.alert('in progress');
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        Alert.alert('play services not available or outdated');
      } else {
        Alert.alert('Something went wrong', error.toString());
        this.setState({
          error,
        });
      }
    }
  };

  _signOut = async () => {
    try {
      await GoogleSignin.revokeAccess();
      await GoogleSignin.signOut();

      this.setState({ userInfo: null, error: null });
    } catch (error) {
      this.setState({
        error,
      });
    }
  };
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  userInfo: { fontSize: 18, fontWeight: 'bold', marginBottom: 20 },
  pageContainer: { flex: 1 },
});

export default withNavigationFocus(GoogleSigninSampleApp)
//AppRegistry.registerComponent('GoogleSigninSampleApp', () => GoogleSigninSampleApp);