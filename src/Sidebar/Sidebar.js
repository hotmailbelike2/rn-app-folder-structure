import React from 'react'
import {AppRegistry, Image, StatusBar} from 'react-native'
import {Container, Content, Text, List, ListItem, Icon, Left} from 'native-base'
import styles from './styles'

const routes = [
  {name: 'Order', route: 'OrderView'},
  {name: 'Items', route: 'ItemsView'},
  {name: 'Report', route: 'ReportView'},
  {name: 'Settings', route: 'SettingsView'},
  {name: 'Info', route: 'InfoView'},

]


export default class Sidebar extends React.Component {
  render() {
    return (
      <Container>
        <Content>
          <List
            dataArray={routes}
            renderRow={data => {
              return (
                <ListItem
                  button
                  onPress={() =>{ this.props.navigation.navigate(data.route); this.props.navigation.closeDrawer() } }
                >
                  <Left>
                   
                    <Text style={{color: '#764443'}}>{data.name}</Text>
                  </Left>
                </ListItem>
              )
            }}
          />
        </Content>
      </Container>
    )
  }
}
