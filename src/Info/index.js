import React, {Component} from 'react'
import InfoView from './InfoView.js'
import { createStackNavigator } from 'react-navigation-stack'
import { Drawer } from 'native-base'
export default (Drawnav = createStackNavigator(
  {
    InfoView: {screen: InfoView},
  },

  {
    initialRouteName: 'InfoView',
    // headerMode: 'none',
  },
))
// export default  InfoView
