import React, {Component} from 'react'
import ReportView from './ReportView.js'
import { createStackNavigator } from 'react-navigation-stack'
import { Drawer } from 'native-base'
export default (Drawnav = createStackNavigator(
  {
    ReportView: {screen: ReportView},
  },

  {
    initialRouteName: 'ReportView',
    // headerMode: 'none',
  },
))
// export default  ReportView
