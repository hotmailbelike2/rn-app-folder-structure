import React from 'react';
import { Image, StatusBar } from 'react-native'
import {
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Form,
  List,
  Left,
  Right,
  Icon,
  Title,
  Button,
  Text,
  Input,
  Item,
  ListItem,
  Accordion,
  Toast,
  Root
} from 'native-base'
import AsyncStorage from '@react-native-community/async-storage'

import { Col, Row, Grid } from 'react-native-easy-grid'
import { withNavigationFocus } from 'react-navigation'

class ReportView extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      reportOrder: [],
      showAllReport: false,
      currentIndex: ''
    }
  }

  

  render() {
    return (
      <>
        <Container>
          <Content>
            
          </Content>
        </Container>

      </>
    )
  }
}

ReportView.navigationOptions = ({ navigation }) => ({
  header: (
    <Header style={{ backgroundColor: '#000000' }}>
      <StatusBar backgroundColor="#000000" barStyle="light-content" />
      <Left>
        <Button transparent onPress={() => navigation.openDrawer()}>
          <Icon name="menu" />
        </Button>
      </Left>
      <Body>
        <Title>Report</Title>
      </Body>
      <Right />
    </Header>
  ),
})
export default withNavigationFocus(ReportView)