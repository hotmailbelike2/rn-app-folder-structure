import React from 'react';
import { Image, TouchableOpacity, StatusBar } from 'react-native'
import AsyncStorage from '@react-native-community/async-storage'
import {
  Container,
  Card,
  CardItem,
  Body,
  Content,
  Header,
  Form,
  List,
  Left,
  Right,
  Icon,
  Title,
  Button,
  Text,
  Input,
  Item,
  ListItem,
  Accordion,
  Toast,
  Root
} from 'native-base'

import { Col, Row, Grid } from 'react-native-easy-grid'
import { withNavigationFocus } from 'react-navigation'

class ItemsView extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      itemList: [],
      search: '',
      filteredList: [],
      currentIndex: null,
      showToast: false

    }
  }
  render() {

    return (
      <Root>

        <Container>
          <Content>
              <Text style={{ color: 'grey', textAlign: 'center', marginTop: 5 }}>
                No items have been added
            </Text>
          </Content>
        </Container>
      </Root>
    )
  }
}

ItemsView.navigationOptions = ({ navigation }) => ({
  header: (
    <Header style={{ backgroundColor: '#ffffff' }}>
      <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
      <Left>
        <Button transparent onPress={() => navigation.openDrawer()}>
          <Icon style={{ color: '000000' }} name="menu" />
        </Button>
      </Left>
      <Body>
        <Title style={{ color: '000000' }}>Items</Title>
      </Body>
      <Right />
    </Header>
  ),
})

export default withNavigationFocus(ItemsView)