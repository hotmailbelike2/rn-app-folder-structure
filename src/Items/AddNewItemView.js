import React from 'react';
import { Image, StatusBar, findNodeHandle } from 'react-native'
import {
    Container,
    Card,
    CardItem,
    Body,
    Content,
    Item,
    Label,
    Input,
    Header,
    Left,
    Right,
    Form,
    Icon,
    Title,
    Button,
    Text,
} from 'native-base'

import AsyncStorage from '@react-native-community/async-storage'

import { Col, Row, Grid } from 'react-native-easy-grid'
import { TextInput } from 'react-native-gesture-handler';

export default class AddNewItemViewView extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            newItemName: '',
            newItemBuyingPrice: 0,
            newItemSellingPrice: 0,
            itemList: [],
            validateName: false,
            validateNumber: false
        }
    }

    componentDidMount = () => {
        this.getData()
    }

    static navigationOptions = {
        drawerLabel: 'AddNewItemView',
        drawerIcon: ({ tintColor }) => (
            <Image
                // source={require('../../assets/Blackbird.jpg')}
                //   style={[styles.icon, { tintColor: tintColor }]}
                style={{ width: 30, height: 30 }}
            />
        ),
    };

    
    /*
    retrieves from async
    */
    getData = async () => {
        try {
            const value = await AsyncStorage.getItem('itemList')
            if (value !== null) {
                this.setState({ itemList: JSON.parse(value) })
            }
        } catch (e) {
            // error reading value
        }
    }
    
    /*
    updates async
    */
    storeData = async (itemList) => {
        try {
            await AsyncStorage.setItem('itemList', JSON.stringify(itemList))
            alert("stored " + JSON.stringify(itemList))
            this.props.navigation.navigate('ItemsView')
        } catch (e) {
            console.log("did not store")
        }
    }
    /*
    validates Item name input
    */
    checkNameInput = (text, fieldName) => {
        if (text.trim() != '') {
            this.setState({
                [fieldName]: text, validateName: true
            })
        }
        else {
            alert("Name can not be empty")
        }
    }
    /*
    validates Item buying and selling price input
    */
    checkNumberInput = (text, fieldName) => {
        if (text >= 0 && text.trim() != '') {
            this.setState({
                [fieldName]: text, validateNumber: true
            })
        }
        else {
            alert('Invalid input')
        }

    }
    
    /*
    used to handle textInput for different fields
    */
    handleChangeInput = (text, fieldName) => {
        if (fieldName === 'newItemName') {
            this.checkNameInput(text, fieldName)
        }
        else {
            this.checkNumberInput(text, fieldName)
        }
    }
    
    /*
    pushes new item after checking if it pre exists
    */
    newItemEntry = (newItem) => {
        let itemList = this.state.itemList
        itemList.push(newItem)
        this.storeData(itemList)
    }

    
    /*
    checks if new item prexists within itemList
    */
    checkItemExists = (newItem) => {
        const normalizedText = newItem.newItemName.toLowerCase()
        const listOfItems = this.state.itemList
        const result = listOfItems.filter(function (e) {
            if (e.newItemName.toLowerCase() === normalizedText) {
                alert('Item exists')
                return e.newItemName
            }     
        })
        if(result.length ==0){
            this.newItemEntry(newItem)
        }
    }

    
    /*
    creates newItem object with textInputs and passes for validationCheck
    */
    onSubmit = () => {
        let newItem = {
            newItemName: this.state.newItemName,
            newItemBuyingPrice: this.state.newItemBuyingPrice,
            newItemSellingPrice: this.state.newItemSellingPrice
        }
        if (this.state.validateName == true && this.state.validateNumber == true) {
            this.checkItemExists(newItem)
        }
        else {
            alert('Input Not Valid')
        }
    }

    render() {
        return (
            <>
                <Container>
                    <Content>
                        <Form>

                            <Item floatingLabel>
                                <Label>Name</Label>
                                <Input onChangeText={(text) => { this.handleChangeInput(text, 'newItemName') }} />
                            </Item>
                            <Item floatingLabel >
                                <Label>Buying Price </Label>
                                <Input keyboardType="numeric" onChangeText={(text) => { this.handleChangeInput(text, 'newItemBuyingPrice') }} />
                            </Item>
                            <Item floatingLabel>
                                <Label>Selling Price</Label>
                                <Input keyboardType="numeric" onChangeText={(text) => { this.handleChangeInput(text, 'newItemSellingPrice') }} />
                            </Item>

                        </Form>
                    </Content>
                    <Button block success onPress={() => { this.onSubmit() }}>
                        <Text>Submit</Text>
                    </Button>
                </Container>
            </>
        )
    }
}

AddNewItemViewView.navigationOptions = ({ navigation }) => ({
    header: (
        <Header style={{ backgroundColor: '#ffffff' }}>
            <StatusBar backgroundColor="#ffffff" barStyle="dark-content" />
            <Left>
                <Button transparent onPress={() => navigation.openDrawer()}>
                    <Icon style={{ color: '000000' }} name="menu" />
                </Button>
            </Left>
            <Body>
                <Title style={{ color: '000000' }}>Add New Item</Title>
            </Body>
            <Right />
        </Header>
    ),
})
